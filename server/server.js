const app = require('express')();
// const express = require('express');
// const app = express();
const port = process.env.port || 3040;
require('dotenv').config();
// =============== BODY PARSER SETTINGS =====================
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// =============== DATABASE CONNECTION =====================
const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
async function connecting() {
	try {
		//'mongodb://127.0.0.1/test_mongodb'
		await mongoose.connect(process.env.MONGO, { useUnifiedTopology: true, useNewUrlParser: true });
		console.log('Connected to the DB');
	} catch (error) {
		console.log('ERROR: Seems like your DB is not running, please start it up !!!');
	}
}
connecting();
// var cron = require('node-cron');
// cron.schedule('* * * * * *', () => {
// 	console.log('running a task every second');
//   });
// ADMIN BRO
// importing schema
const Users = require('./models/users.models.js');
const Plants = require('./models/plants.models.js');

const AdminBro = require('admin-bro')
const AdminBroExpressjs = require('admin-bro-expressjs')
// We have to tell AdminBro that we will manage mongoose resources with it
AdminBro.registerAdapter(require('admin-bro-mongoose'))
// Pass all configuration settings to AdminBro
const adminBro = new AdminBro({
  resources: [Users,Plants],
  rootPath: '/admin'
})

// Build and use a router which will handle all AdminBro routes
const router = AdminBroExpressjs.buildRouter(adminBro)
app.use(adminBro.options.rootPath, router)
// END ADMIN BRO
//================ CORS ================================
const cors = require('cors');
app.use(cors());
// =============== ROUTES ==============================
app.use('/users', require('./routes/users.routes'));
app.use('/plants', require('./routes/plants.routes'));

// =============== START SERVER =====================
app.listen(port, () => console.log(`server listening on port ${port}`));