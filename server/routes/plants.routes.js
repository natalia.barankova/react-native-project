const express = require('express');
const router = express.Router();
const controller = require('../controllers/plants.controllers');

router.get('/', controller.getAllPlants);
router.post('/create', controller.createPlant);

router.delete('/delete/:plantId', controller.deletePlants);
module.exports = router;