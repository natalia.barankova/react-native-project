const mongoose = require('mongoose');

const plantsSchema = new mongoose.Schema({
    name: { type: String, required: true },
    type: { type: String, required: true },
    image: { type: String, required: true },
    difficulty: { type: String, required: true },
    fertiliseFrequency: { type: String, required: true },
    repottingFrequency: { type: String, required: true },
    wateringFrequency: { type: String, required: true },
    mistingFrequency: { type: String, required: true },
});

module.exports = mongoose.model('Plants', plantsSchema);
