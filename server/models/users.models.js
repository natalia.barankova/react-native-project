const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    email: { type: String, unique: true, required: true },
    password: { type: String, required: true },
    plants: [
        {
            name: { type: String },
            type: { type: String },
            image: { type: String },
            difficulty: { type: String },
            fertiliseFrequency: { type: String },
            repottingFrequency: { type: String },
            wateringFrequency: { type: String },
            mistingFrequency: { type: String },
            room: { type: String },
            size: { type: String },
            lastWateringDay: { type: Date, default: Date.now },
            firstDay: { type: Date, default: Date.now }
        }
    ],
});
module.exports = mongoose.model('users', userSchema);