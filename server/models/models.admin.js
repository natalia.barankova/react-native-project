const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const usersSchema = new Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    encryptedPassword: {
        type: String,
        required: true
    },
    admin: {
        type: Boolean,
        default: false
    }
})
module.exports =  mongoose.model('admin', usersSchema);