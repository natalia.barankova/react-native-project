const argon2 = require('argon2');
const Users = require('../models/models.admin');

const register = async (req, res) => {
	try {
        const { email, password } = req.body;
        const hash = await argon2.hash(password);
        const newUser = await Users.create({
            email,
            encryptedPassword: hash,
            admin: true // comment this line after creating the first user
        })
        res.send(newUser)
	} catch (error) {
        res.send({error})
		console.log('error ===>', error);
	}
};

module.exports = {
	register
};
