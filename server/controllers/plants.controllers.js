const Plants = require("../models/plants.models");

const getAllPlants = async (req, res) => {
  try {
    const plants = await Plants.find({});
    res.send(plants);
  } catch (e) {
    res.send({ e });
  }
};

const createPlant = async (req, res) => {
  let {
   name,
   type,
    image,
   difficulty,
   fertiliseFrequency,
   repottingFrequency,
   wateringFrequency,
   mistingFrequency
   
  } = req.body;
  try {
    const created = await Plants.create({
        name,
        type,
         image,
        difficulty,
        fertiliseFrequency,
        repottingFrequency,
        wateringFrequency,
        mistingFrequency
        
    });
    res.send(created);
  } catch (e) {
    res.send({ e });
  }
};

const deletePlants = async (req, res) => {
  let { plantId } = req.params;
  try {
    const removed = await Plants.deleteOne({ _id: plantId });
    res.send({ removed });
  } catch (error) {
    res.send({ error });
  }
};


const updatePlant = async (req, res) => {
  let { plant, newPlant } = req.body;
  try {
    const updated = await Plants.updateOne(
      { plant },
      { plant: newPlant }
    );
    res.send({ updated });
  } catch (error) {
    res.send({ error });
  }
};

module.exports = {
  getAllPlants,
  createPlant,
  deletePlants,
  updatePlant,
};
