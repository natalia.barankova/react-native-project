const AdminBro = require('admin-bro');
const AdminBroMongoose = require('admin-bro-mongoose');
const UsersAdmin = require('../models/models.admin'); // replace this for your model
const Plants = require('../models/plants.models');// change the path for your users model path

AdminBro.registerAdapter(AdminBroMongoose);

const options = {
	resources: [ UsersAdmin, Plants ] // the models must be included in this array
};

module.exports = options;
