import React from 'react'
import Begin from './components/Begin.js';
import PlantsList from './components/PlantsList.js';
import Tasks from './components/Tasks.js';
import Profile from './components/Profile.js'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { View } from 'react-native';

export default function Router(props) {
  const Tab = createBottomTabNavigator();

  return (
    <View style={{ flex: 1 }}>
      <NavigationContainer>
        <Tab.Navigator >
          <Tab.Screen
            name="PlantsList"
            options={{ bottomBarLabel: 'PlantsList' }}
            component={PlantsList}
          />
          <Tab.Screen
            name="Profile"
            options={{ bottomBarLabel: 'Profile' }}
            component={() => <Profile logout={props.logout} />}
          />
          <Tab.Screen
            name="Tasks"
            component={Tasks}
            options={{ bottomBarLabel: 'Tasks' }}
          />
          <Tab.Screen
            name="Begin"
            component={Begin}
          />
        </Tab.Navigator>
      </NavigationContainer>
    </View >
  )
}
