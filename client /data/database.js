const database= [
    {
        image: 'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625481684/_vyr_31_Monstera_deliciosa_maxi2_gkte0j.jpg',
        name: 'Monstera deliciosa',
        difficulty: 'easy', 
        waterAmount_big: '300ml',
        waterAmount_medium: '200ml', 
        waterAmount_small: '100ml',
        fertilisingAmount_big: '300g',
        fertilisingAmount_medium: '200g', 
        fertilisingAmount_small: '100g',

    },
    {
        image: 'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625482035/Dracaena_20Warneckii_20_peqhfs.png',
        name: 'Dracaena Warneckii',
        difficulty: 'easy', 
        waterAmount_big: '300ml',
        waterAmount_medium: '200ml', 
        waterAmount_small: '100ml',
        fertilisingAmount_big: '300g',
        fertilisingAmount_medium: '200g', 
        fertilisingAmount_small: '100g',

    },
    {
        image: 'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625482035/Echeveria_20Preta_ssyj9c.png',
        name: 'Echeveria Preta',
        difficulty: 'easy', 
        waterAmount_big: '300ml',
        waterAmount_medium: '200ml', 
        waterAmount_small: '100ml',
        fertilisingAmount_big: '300g',
        fertilisingAmount_medium: '200g', 
        fertilisingAmount_small: '100g',

    }
    ,
    {
        image: 'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625482036/Snake_20Plant_20_wqbkbc.png',
        name: 'Sansevieria',
        difficulty: 'easy', 
        waterAmount_big: '300ml',
        waterAmount_medium: '200ml', 
        waterAmount_small: '100ml',
        fertilisingAmount_big: '300g',
        fertilisingAmount_medium: '200g', 
        fertilisingAmount_small: '100g',

    },

    {
        image: 'uri: https://res.cloudinary.com/dqaeevcqf/image/upload/v1625482036/Pothos_20Hanging_20Plant_qcmw1s.png',
        name: 'Pothos Hanging Planť',
        difficulty: 'easy', 
        waterAmount_big: '300ml',
        waterAmount_medium: '200ml', 
        waterAmount_small: '100ml',
        fertilisingAmount_big: '300g',
        fertilisingAmount_medium: '200g', 
        fertilisingAmount_small: '100g',

    },

    {
        image: 'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625482036/Pink_Anthurium_tm9ftl.png',
        name: 'Pink Anthurium',
        difficulty: 'easy', 
        waterAmount_big: '300ml',
        waterAmount_medium: '200ml', 
        waterAmount_small: '100ml',
        fertilisingAmount_big: '300g',
        fertilisingAmount_medium: '200g', 
        fertilisingAmount_small: '100g',

    }

]