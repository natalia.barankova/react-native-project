import React, { useState } from 'react';
import Login from './components/Login.js';
import Register from './components/Register.js';
import Home from './components/Home.js';

export default function Authentication(props) {
  const [page, setPage] = useState("home")

  if (page === "home") {
    return <Home setPage={(page) => setPage(page)} />
  } else if (page === "login") {
    return <Login setPage={(page) => setPage(page)} login={(token) => props.login(token)} />
  } else if (page === "register") {
    return <Register setPage={(page) => setPage(page)} login={(token) => props.login(token)} />
  }
}