import 'react-native-gesture-handler';
import React, { useState, useEffect } from 'react';
//import { AsyncStorage } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { URL } from './config';
import axios from 'axios';
import Router from './Router';
import Authentication from './Authentication';

export default function App() {
	const [isLoggedIn, setIsLoggedIn] = useState(false);

	useEffect(() => {
		const handleToken = async () => {
			try {
				const token = JSON.parse(await AsyncStorage.getItem('token'));
				if (token === null) return setIsLoggedIn(false);
				axios.defaults.headers.common['Authorization'] = token;
				const response = await axios.post(`${URL}/users/verify_token`);
				return response.data.ok ? setIsLoggedIn(true) : setIsLoggedIn(false);
			}
			catch (err) {
				console.error(err)
			}
		}
		handleToken()
	}, [])

	const login = async (token) => {
		try {
			await AsyncStorage.setItem('token', JSON.stringify(token));
			setIsLoggedIn(true);
		}
		catch (error) {
			console.error(error)
		}
	};

	const logout = async () => {
		try {
			AsyncStorage.removeItem('token');
			setIsLoggedIn(false);
		}
		catch (error) {
			console.error(error)
		}
	};

	if (!isLoggedIn) {
		return (
			<Authentication isLoggedIn={isLoggedIn} setIsLoggedIn={(bool) => setIsLoggedIn(bool)} login={(token) => login(token)} />
		)
	} else {
		return (
			<Router logout={logout} />
		);
	}
}
