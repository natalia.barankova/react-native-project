import React from 'react';
import { StyleSheet, Text, View, ImageBackground, Button } from 'react-native';
import { useFonts } from 'expo-font';

export default function Home(props) {
  const [loaded, error] = useFonts({ ClashRegular: require('../assets/fonts/ClashDisplay-Regular.ttf') });

  return (
    <View style={styles.backgroundImage}>
      <ImageBackground source={{
        uri: 'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625152463/home_us10jr.jpg'
      }} style={styles.backgroundImage}
      >
        <View style={styles.container}>
          <Button
            title="Login"
            onPress={() => props.setPage("login")}
          />
        </View>
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
  },
  container: {
    flex: 1
  },
  headlines: {
    top: '40%',
    letterSpacing: 1.2,
    flex: 1,
    fontFamily: 'ClashRegular',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 35,
    color: '#98FAE0',
    flex: 1,
    zIndex: 3
  }
});

