import React, { useState } from 'react';
import { StyleSheet, Text, View, ImageBackground, TextInput, Image, FlatList, TouchableOpacity } from 'react-native';
import 'react-native-gesture-handler';
import Modal from "react-native-modalbox";
import { useFonts } from 'expo-font';
import { AntDesign } from '@expo/vector-icons';

const database2 = [
    'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625481684/Pink_Anthurium_g4hz6k.png',
    'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625481684/Pink_Anthurium_g4hz6k.png',
    'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625481684/Pink_Anthurium_g4hz6k.png',
    'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625481684/Pink_Anthurium_g4hz6k.png'
]

const database = [
    {
        key: '6',
        img: 'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625481684/_vyr_31_Monstera_deliciosa_maxi2_gkte0j.jpg',
        name: 'Monstera',
        type: 'Deliciosa',
        difficulty: 'easy',
        waterAmount_big: '300ml',
        waterAmount_medium: '200ml',
        waterAmount_small: '100ml',
        fertilisingAmount_big: '300g',
        fertilisingAmount_medium: '200g',
        fertilisingAmount_small: '100g',
    },
    {
        key: '5',
        img: 'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625481686/Dracaena_20Warneckii_20_ddwsz6.png',
        name: 'Dracaena',
        type: 'Warneckii',
        difficulty: 'easy',
        waterAmount_big: '300ml',
        waterAmount_medium: '200ml',
        waterAmount_small: '100ml',
        fertilisingAmount_big: '300g',
        fertilisingAmount_medium: '200g',
        fertilisingAmount_small: '100g',
    },
    {
        key: '4',
        img: 'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625481684/Pink_Anthurium_g4hz6k.png',
        name: 'Echeveria',
        type: 'Preta',
        difficulty: 'easy',
        waterAmount_big: '300ml',
        waterAmount_medium: '200ml',
        waterAmount_small: '100ml',
        fertilisingAmount_big: '300g',
        fertilisingAmount_medium: '200g',
        fertilisingAmount_small: '100g',
    }
    ,
    {
        key: '3',
        img: 'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625481684/Pink_Anthurium_g4hz6k.png',
        name: 'Sansevieria',
        type: 'Francisii',
        difficulty: 'easy',
        waterAmount_big: '300ml',
        waterAmount_medium: '200ml',
        waterAmount_small: '100ml',
        fertilisingAmount_big: '300g',
        fertilisingAmount_medium: '200g',
        fertilisingAmount_small: '100g',
    },

    {
        key: '1',
        img: 'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625481683/Pothos_20Hanging_20Plant_ce4qt6.png',
        name: 'Pothos',
        type: 'Marble Queen',
        difficulty: 'easy',
        waterAmount_big: '300ml',
        waterAmount_medium: '200ml',
        waterAmount_small: '100ml',
        fertilisingAmount_big: '300g',
        fertilisingAmount_medium: '200g',
        fertilisingAmount_small: '100g',
    },
    {
        key: '2',
        img: 'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625481684/Pink_Anthurium_g4hz6k.png',
        name: 'Pink',
        type: 'Superbum',
        difficulty: 'easy',
        waterAmount_big: '300ml',
        waterAmount_medium: '200ml',
        waterAmount_small: '100ml',
        fertilisingAmount_big: '300g',
        fertilisingAmount_medium: '200g',
        fertilisingAmount_small: '100g',
    },
    {
        key: '2',
        img: 'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625481684/Pink_Anthurium_g4hz6k.png',
        name: 'Anthurium',
        type: 'Andraeanum',
        difficulty: 'easy',
        waterAmount_big: '300ml',
        waterAmount_medium: '200ml',
        waterAmount_small: '100ml',
        fertilisingAmount_big: '300g',
        fertilisingAmount_medium: '200g',
        fertilisingAmount_small: '100g',
    }
]

export default function Plants() {

    const [show, setShow] = useState(false)
    const [search, setSearch] = useState('')
    const [showPlant, setShowPlant] = useState(false)
    const [plant, setPlant] = useState({
        key: database.key,
        img: database.img,
        name: database.name,
        type: database.type,
        difficulty: database.difficulty
    })
    const [plantWithSize, setPlantWithSize] = useState({
        key: database.key,
        img: database.img,
        name: database.name,
        type: database.type,
        difficulty: database.difficulty
    })
    const [myPlantsList, setMyPlantsList] = useState([])

    useFonts({ ClashRegular: require('../assets/fonts/ClashDisplay-Regular.ttf') });
    useFonts({ ClashMedium: require('../assets/fonts/ClashDisplay-Medium.ttf') });

    const theme = {
        colors: {
            primary: '#98FAE0',
            secondary: '#0C2F22'
        }
    }

    const Item = ({ item }) => {
        return (
            <View style={styles.listItem}>
                <Image style={styles.mappedImage} source={{ uri: item.img }}></Image>
            </View>
        );
    }

    /* const addPlant= (plant)=>{
        const newPlant= plant
        const index= newPlant.findIndex((idx)=>idx.name=== plant.name && plant.name);
        if (index!==-1){
            setPlant()
        }
    }  */

    const addSize = (givensize) => {
        const newSize = givensize;
        setPlantWithSize({ ...plant, ['size']: newSize })
    }

    const updateSearch = (text) => {
        setSearch(text);
    };

    const filteredPlants = database.filter(
        (plant) => {
            return (plant.name.toLowerCase().indexOf(search.toLowerCase()) !== -1
                || plant.type.toLowerCase().indexOf(search.toLowerCase()) !== -1)
        }
    );

    const addPlantToList = (chosenPlant) => {
        const newList = [...myPlantsList]
        newList.push(chosenPlant)
        setMyPlantsList(newList)
    }

    const sendPlantToList = async (e) => {
        e.preventDefault();
        try {
            const response = await axios.post(`${URL}/users/addPlant`, { plantWithSize });
            setMessage(response.data.message);
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <View style={styles.backgroundImage}>
            <ImageBackground source={{
                uri: 'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625649866/backgroundsearch_raq6qn.png'
            }} style={styles.backgroundImage}
            >
                <View style={styles.container}>
                    <TouchableOpacity onPress={() => setShow(true)}>
                        <Image
                            source={{ uri: 'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625561533/Group_19_oahiue.png' }}
                            style={styles.mappedImage}
                        />
                        <Text style={styles.text}>
                            Add Item
                        </Text>

                    </TouchableOpacity>
                </View>

                <FlatList
                    style={{ flex: 1 }}
                    data={myPlantsList}
                    numColumns={2}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item }) => {
                        return (
                            <View>
                                <View style={styles.container}>
                                    <View >
                                        {item.img && (
                                            <Image
                                                source={{ uri: item.img }}
                                                style={styles.mappedImage}
                                            />
                                        )}
                                        {!!item.name && (
                                            <View style={
                                                styles.search2
                                            }>
                                                <Text
                                                    style={{
                                                        fontFamily: 'ClashMedium',
                                                        paddingVertical: 10,
                                                        fontSize: 15,
                                                        marginLeft: 10,
                                                        color: '#98FAE0',
                                                    }}>
                                                    {item.name}
                                                    <AntDesign name="select1" margin={10} size={22} color='#98FAE0' />
                                                </Text>
                                            </View>
                                        )}
                                        {!!item.type && (
                                            <Text
                                                style={{
                                                    fontFamily: 'ClashMedium',
                                                    fontSize: 10,
                                                    marginTop: -5,
                                                    paddingLeft: 10,
                                                    color: '#98FAE0',
                                                }}>
                                                {item.type}
                                            </Text>
                                        )}
                                    </View>
                                </View>
                            </View>
                        );
                    }}
                />
                <Modal
                    animationType="slide"
                    swipeDirection="left"
                    swipeToClose={true}
                    isOpen={show}
                    transparent={false}
                    visible={show}
                    style={{
                        backgroundColor: "#98FAE0", flex: 1,
                        width: '85%',
                        marginBottom: 130,
                    }}
                >
                    <View style={{ flex: 1 }}>
                        <View >
                            <AntDesign name="close" style={{ margin: 10 }} onPress={() => setShow(false)} size={32} color="black" />
                            <View>
                                <View style={styles.search}>
                                    <Text style={styles.searchedText} >
                                        Search Plant
                                    </Text>
                                </View>
                                <TextInput onChangeText={(text) => updateSearch(text)}
                                    placeholder={'name of the plant'} style={styles.input}>
                                </TextInput>
                                {filteredPlants.map((item, idx) => {
                                    return <View key={idx} style={styles.search}>
                                        <View>
                                            <Text style={styles.searchedText} key={idx} >
                                                {item.name}
                                            </Text>
                                            <Text style={styles.searchedTextMin} key={idx} >
                                                {item.type}
                                            </Text>
                                        </View>
                                        <View>
                                            <AntDesign onPress={() => {
                                                setShowPlant(true);
                                                setPlant({
                                                    key: item.key,
                                                    img: item.img,
                                                    name: item.name,
                                                    type: item.type,
                                                    difficulty: item.difficulty
                                                })
                                            }}
                                                name="rightcircle" size={20} color="black" />
                                        </View>
                                    </View>
                                })}
                            </View>
                        </View>
                        <Modal
                            animationType="slide"
                            swipeDirection="left"
                            swipeToClose={true}
                            isOpen={showPlant}
                            transparent={false}
                            visible={showPlant}
                            style={{
                                backgroundColor: "#98FAE0",
                                flex: 1,
                                width: '100%',
                                marginTop: 0,
                                marginBottom: 0
                            }}
                        >
                            <View style={{ flex: 1 }} >
                                <AntDesign name="close" style={{ margin: 10 }} onPress={() => { setShowPlant(false), setShow(false) }} size={32} color="black" />
                                <View style={styles.search3}>
                                    <View>
                                        <Text style={styles.profile} >
                                            {plant.name} </Text>
                                        <Text style={styles.profileMin} >
                                            {plant.type} </Text>
                                        <Text style={styles.profileMin} >
                                            Difficulty: {plant.difficulty} </Text>
                                    </View>
                                    <View>
                                        <Image style={styles.mappedImageProfile} source={{ uri: plant.img }} >
                                        </Image>
                                    </View>
                                </View>
                                <View style={styles.addPlant}>
                                    <View >
                                        <Text style={styles.profile} >
                                            Add size </Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                        <TouchableOpacity value='small' onPress={() => addSize('mini')}>
                                            <Image
                                                source={{ uri: 'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625645830/plant1_f6dttd.png' }}
                                                style={styles.sizeIcon}
                                            />
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => addSize('small')}>
                                            <Image
                                                source={{ uri: 'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625645830/plantt4_fy8thp.png' }}
                                                style={styles.sizeIcon}
                                            />
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => addSize('medium')}>
                                            <Image
                                                source={{ uri: 'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625645830/plantt3_el5gnp.png' }}
                                                style={styles.sizeIcon}
                                            />
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => addSize('large')}>
                                            <Image
                                                source={{ uri: 'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625645830/plantt2_uwj99b.png' }}
                                                style={styles.sizeIcon}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={styles.addPlant}>
                                    <View>
                                        <Text style={styles.profile} >
                                            Add room </Text>
                                    </View>
                                </View>
                                <View style={styles.container2}>
                                    <AntDesign name="leftcircle" style={styles.arrow}
                                        onPress={() => { setShowPlant(false) }}
                                        size={32} color="black" />
                                    <AntDesign onPress={() => { addPlantToList(plantWithSize); sendPlantToList(plantWithSize) }} name="pluscircleo" size={24} color="black" />
                                </View>
                            </View>
                        </Modal>
                    </View>
                </Modal>
            </ImageBackground>
        </View>
    );
}

const styles = StyleSheet.create({
    searchedText: {
        fontFamily: 'ClashMedium',
        fontSize: 18,
    },
    searchedTextMin: {
        fontFamily: 'ClashMedium',
        fontSize: 12,
    },
    profile: {
        fontFamily: 'ClashMedium',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        justifyContent: 'space-between',
        fontSize: 21,
        letterSpacing: 1.1,
    },
    profileMin: {
        fontFamily: 'ClashRegular',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        justifyContent: 'space-between',
        fontSize: 12,
        marginTop: 5,
        letterSpacing: 1.1
    },
    search: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        justifyContent: 'space-between',
        marginLeft: 50,
        marginRight: 50,
        marginBottom: 0,
        paddingBottom: 10,
        paddingTop: 10,
        alignItems: 'center',
        top: 50,
        borderBottomColor: '#000000',
        borderBottomWidth: 1,
    },
    search3: {
        flex: 2,
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
        marginRight: 30,
        marginTop: 40,
        paddingRight: 30,
        paddingLeft: 30,
    },
    addPlant: {
        flex: 1,
        flexDirection: 'column',
        width: '100%',
        justifyContent: 'center',
        alignContent: 'center',
        marginRight: 30,
        marginTop: 40,
        paddingRight: 30,
        paddingLeft: 30,
    },
    search2: {
        flexDirection: 'column',
    },
    container: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: 190,
        justifyContent: 'space-between',
        alignItems: 'center',
        margin: 10
    },
    container2: {
        flex: 1,
        flexDirection: 'column',
        width: '100%',
        justifyContent: 'flex-end',
        alignContent: 'center',
        alignItems: 'center',
        marginRight: 30,
        marginBottom: 50,
    },
    containerColumn: {
        flexDirection: 'column',
        flexWrap: 'wrap',
        flex: 0.5,
        width: '100%',
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
    },
    text: {
        fontFamily: 'ClashRegular',
        color: '#98FAE0',
        marginLeft: 10,
    },
    mappedImage: {
        margin: 10,

        width: 180,
        height: 300
    },
    sizeIcon: {
        width: 34,
        height: 75,
        marginTop: 30
    },
    mappedImageProfile: {
        marginTop: -60,
        width: 140,
        height: 250
    },
    input: {
        borderColor: 'black',
        borderWidth: 1,
        fontFamily: 'ClashRegular',
        margin: '5%',
        top: 50,
        paddingLeft: '3%',
        alignSelf: 'center',
        width: '75%',
        height: 50
    }
}
);
