import React, { useState } from 'react';
import { StyleSheet, Text, View, ImageBackground, TextInput, Button } from 'react-native';
import * as Font from 'expo-font';
import { useFonts } from 'expo-font';
import axios from 'axios';
import { ScrollView } from 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { URL } from '../config';
import { StatusBar } from 'expo-status-bar';

const Register = (props) => {
	const [message, setMessage] = useState('');
	const [form, setValues] = useState({
		email: '',
		password: '',
		password2: ''
	});

	const handleChange = (text, name) => {
		setValues({ ...form, [name]: text });
	};

	const handleSubmit = async (e) => {
		e.preventDefault();
		try {
			const response = await axios.post(`${URL}/users/register`, {
				email: form.email,
				password: form.password,
				password2: form.password2
			});
			setMessage(response.data.message);
			if (response.data.ok) {
				setTimeout(() => {
					props.login(response.data.token)
				}, 2000);
			}
		} catch (error) {
			console.error(error);
		}
	};

	return (
		<View style={styles.backgroundImage}>
			<StatusBar style="dark" />
			<ImageBackground source={{
				uri: 'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625128117/Untitled-1_p4odos.jpg'
			}} style={styles.backgroundImage}
			>
				<View style={styles.container}>
					<Text style={styles.headlines}>Create an account</Text>
					<TextInput
						placeholderTextColor="white"
						placeholder="Email"
						style={styles.input}
						onChangeText={(text) => handleChange(text, 'email')}
						value={form.email.toLowerCase()}
					/>
					<TextInput
						placeholderTextColor="white"
						placeholder="Password"
						style={styles.input}
						onChangeText={(text) => handleChange(text, 'password')}
					/>
					<TextInput
						placeholderTextColor="white"
						placeholder="Password"
						style={styles.input}
						onChangeText={(text) => handleChange(text, 'password2')}
					/>
					<Button title="Go to Log In" onPress={() => props.setPage("login")} />
					<Button title='Press' onPress={handleSubmit}
						onSubmitEditing={handleSubmit} >
					</Button>
				</View>
				<Text style={styles.message}>{message}</Text>
			</ImageBackground>
		</View>
	);

};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		height: '100%',
		alignItems: 'center',
		justifyContent: 'center',
		fontFamily: 'ClashRegular',
		color: '#98FAE0'
	},
	backgroundImage: {
		flex: 1,
		resizeMode: 'cover', // or 'stretch'
	},
	headlines: {
		top: '40%',
		letterSpacing: 1.3,
		flex: 1,
		fontFamily: 'ClashRegular',
		alignItems: 'center',
		justifyContent: 'center',
		fontSize: 35,
		color: '#98FAE0',
		flex: 1,
	},
	input: {
		borderColor: "#98FAE0",
		borderWidth: 1,
		fontFamily: 'ClashRegular',
		margin: '3%',
		color: '#98FAE0',
		bottom: '13%',
		width: '75%',
		height: '5%'
	},
	message: {
		color: "white",
		fontSize: 25
	}
});

export default Register;
