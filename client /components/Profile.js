import React from 'react';
import { StyleSheet, View, ImageBackground } from 'react-native';
import { Button, ThemeProvider } from 'react-native-elements';
import 'react-native-gesture-handler';
import { useFonts } from 'expo-font';

export default function Profile(props) {
    useFonts({ ClashRegular: require('../assets/fonts/ClashDisplay-Regular.ttf') });
    useFonts({ ClashMedium: require('../assets/fonts/ClashDisplay-Medium.ttf') });

    const theme = {
        colors: {
            primary: '#98FAE0',
            secondary: '#0C2F22'
        }
    }

    return (
        <ThemeProvider theme={theme}>
            <View style={styles.backgroundImage}>
                <ImageBackground source={{
                    uri: 'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625127300/background_elzsgx.jpg'
                }} style={styles.backgroundImage}
                >
                    <Button title="Log out" onPress={props.logout} />
                </ImageBackground>
            </View>
        </ThemeProvider>
    );
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        fontFamily: 'ClashRegular',
        color: '#98FAE0'
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
        justifyContent: 'center',
    },
});
