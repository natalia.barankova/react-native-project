import { StatusBar } from 'expo-status-bar';
import { NativeRouter, Route, Link } from "react-router-native";
import React from 'react';
import { StyleSheet, Text, View, ImageBackground, Alert, TextInput, TouchableOpacity } from 'react-native';
import { Button, ThemeProvider } from 'react-native-elements';
import 'react-native-gesture-handler';
import * as Font from 'expo-font';
import { useFonts } from 'expo-font';
import CustomButton from '../shared/button.js'
export default function Login({ navigation }) {

    useFonts({ ClashRegular: require('../assets/fonts/ClashDisplay-Regular.ttf') });
    useFonts({ ClashMedium: require('../assets/fonts/ClashDisplay-Medium.ttf') });
    const theme = {
        colors: {
            primary: '#98FAE0',
            secondary: '#0C2F22'
        }
    }
    return (
        <ThemeProvider theme={theme}>
            <View style={styles.backgroundImage}>
                <ImageBackground source={{
                    uri: 'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625127300/background_elzsgx.jpg'
                }} style={styles.backgroundImage}
                >
                    <View style={styles.container}>
                        <Text style={styles.headlines}>Do you have an account?</Text>
                        <TouchableOpacity onPress={() => navigation.navigate('Register')} style={styles.appButtonContainer}>
                            <Text style={styles.appButtonText} >Register</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigation.navigate('Login')} style={styles.appButtonContainer2} >
                            <Text style={styles.appButtonText2}  >Login</Text>
                        </TouchableOpacity>
                    </View >

                </ImageBackground>
            </View>
        </ThemeProvider>
    );
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        fontFamily: 'ClashRegular',
        color: '#98FAE0'

    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
    },
    headlines: {
        top: '40%',
        letterSpacing: 1.3,
        flex: 1,
        fontFamily: 'ClashRegular',
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: 35,
        textAlign: 'center',
        color: '#98FAE0',
        flex: 1,
    },
    appButtonText: {
        fontFamily: 'ClashRegular',
        color: '#98FAE0',
        fontSize: 21

    },
    appButtonContainer: {
        bottom: '20%',

        borderColor: '#98FAE0',
        borderWidth: 1,
        borderRadius: 0,
        paddingVertical: 10,
        paddingHorizontal: 90
    },
    appButtonText2: {
        fontFamily: 'ClashRegular',
        color: '#0C2F22',
        fontSize: 21

    },
    appButtonContainer2: {
        bottom: '15%',
        backgroundColor: '#98FAE0',
        borderWidth: 0,
        borderRadius: 0,
        paddingVertical: 10,
        paddingHorizontal: 106
    }
});
