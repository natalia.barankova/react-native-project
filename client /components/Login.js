import React, { useState } from 'react';
import { StyleSheet, Text, View, ImageBackground, TextInput, Button } from 'react-native';
import { useFonts } from 'expo-font';
import axios from 'axios';
import { URL } from '../config';

export default function Login(props, { navigation }) {
  useFonts({ ClashRegular: require('../assets/fonts/ClashDisplay-Regular.ttf') });
  useFonts({ ClashMedium: require('../assets/fonts/ClashDisplay-Medium.ttf') });

  const [form, setValues] = useState({
    email: '',
    password: ''
  });
  const [message, setMessage] = useState('');

  const handleChange = (text, name) => {
    setValues({ ...form, [name]: text });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post(`${URL}/users/login`, {
        email: form.email.toLowerCase(),
        password: form.password
      });
      setMessage(response.data.message);
      if (response.data.ok) {
        setTimeout(() => {
          props.login(response.data.token);
        }, 2000);
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <View style={styles.backgroundImage}>
      <ImageBackground source={{
        uri: 'https://res.cloudinary.com/dqaeevcqf/image/upload/v1625128117/Untitled-1_p4odos.jpg'
      }} style={styles.backgroundImage}
      >
        <View style={styles.container}>
          <Text style={styles.headlines}>Log In</Text>
          <TextInput
            placeholderTextColor="white"
            placeholder="Email"
            style={styles.input}
            onChangeText={(text) => handleChange(text, 'email')}
            value={form.email.toLowerCase()}
          />
          <TextInput
            placeholderTextColor="white"
            placeholder="Password"
            style={styles.input}
            onChangeText={(text) => handleChange(text, 'password')}
          />
          <Button title="Go to Sign Up" onPress={() => props.setPage("register")} />
          <Button title='Log in' onPress={handleSubmit} onSubmitEditing={handleSubmit} >
          </Button>

          <Text style={styles.message}>{message}</Text>
        </View >
      </ImageBackground>
    </View>
  );
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    fontFamily: 'ClashRegular',
    color: '#98FAE0'
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
  },
  headlines: {
    top: '50%',
    letterSpacing: 1.3,
    flex: 1,
    fontFamily: 'ClashRegular',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 35,
    color: '#98FAE0',
    flex: 1,
  },
  input: {
    borderColor: "#98FAE0",
    borderWidth: 1,
    fontFamily: 'ClashRegular',
    margin: '3%',
    color: '#98FAE0',
    bottom: '13%',
    width: '75%',
    height: '5%',
  },
  message: {
    color: "white",
    fontSize: 25
  }
});

